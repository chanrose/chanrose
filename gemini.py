import os
import json
from datetime import date

file = open('conf-gemini.json')
data = json.load(file)
file.close()
print("Data", data)
bwd = os.getcwd() # store base project working dir
proj_wd = bwd + "/projects" # store projects under the base dir

def get_proj_lists():
    # Return tuple of key and value
    all_projects = dict()
    os.chdir(proj_wd)
    proj_category = os.listdir()
    proj_category.remove(data['template_name'])
    for proj in proj_category:
        os.chdir(f"{proj_wd}/{proj}")
        all_projects[proj] = os.listdir()
    count_project = 0
    sorted_proj = list(all_projects.items())
    sorted_proj.sort(key=lambda x:len(x[1]))
    sorted_proj.reverse()
    return sorted_proj

def read_file(path, file):
    # To return content of the file
    os.chdir(path)
    f_template = open(file)
    temp = f_template.read()
    f_template.close()
    return temp

def write_file(path, file, content):
    # To return content of the file
    os.chdir(path)
    file = open(file, 'w')
    file.write(content)
    file.close()
    print(f"Successfully update {file}")

def get_start_end_tags(*tags):
    temp = []
    for tag in tags:
        temp.extend([f"<!-- start_{tag} -->", f"<!-- end_{tag} -->"])
    return temp

def get_content_w_tag(content, start_tag, end_tag):
    return content[content.find(start_tag):content.find(end_tag) + len(end_tag)]


def update_projects_page():
    sorted_proj = get_proj_lists()
    temp = read_file(bwd, "projects.html")
    sc, ec, st, et, sno, eno = get_start_end_tags("content", "total", "now")
    # To replace this old content to new content
    old_content = get_content_w_tag(temp, sc, ec)
    oc_total = get_content_w_tag(temp, st, et)
    old_date = get_content_w_tag(temp, sno, eno)
    new_content = sc
    count_project = 0
    for key, val in sorted_proj:
        new_content += f"   \n<section >\n   <h2>{key}</h2>  \n<ul>\n"
        if key == "note":
            count_project -= len(val)
        for item in val:
            new_content += f"   <li><a href='./projects/{key}/{item}'>{item[:item.find('.h')]}</a></li>\n"
            count_project += 1
        new_content += "\n</ul>\n</section>"
    new_content += f"\n{ec}"
    temp = temp.replace(old_content, new_content) # Replace old content with new content
    temp = temp.replace(oc_total, f"{st} {count_project} {et}")
    temp = temp.replace(old_date, f"{sno} {date.today().isoformat()} {eno}")
    update_proj = open(f"{bwd}/projects.html", 'w')
    update_proj.write(temp)
    update_proj.close()
    print("Update projects.html completed")


def update_index_page():
    sorted_proj = get_proj_lists()
    # Update Projects Category and Sum
    proj_count = '\n<ul>'
    for item in sorted_proj:
        proj_count += f"\n\t<li>{item[0]}: {len(item[1])} </li>"
    proj_count += '</ul>\n'
    temp = read_file(bwd, "index.html")
    tags = ["recent", "highlighted", "status"]
    sre, ere, shi, ehi, spr, epr, sst, est, sno, eno = get_start_end_tags(tags[0], tags[1], "project_count", tags[2], "now")
    ### important try to optimize it later
    old_re = get_content_w_tag(temp, sre, ere)
    old_hi = get_content_w_tag(temp, shi, ehi)
    old_pr = get_content_w_tag(temp, spr, epr)
    old_st = get_content_w_tag(temp, sst, est)
    old_no = get_content_w_tag(temp, sno, eno)
    # Replace old to new recent, highlighted, status, count
    temp = temp.replace(old_pr, f"{spr} {proj_count} {epr}")
    temp = temp.replace(old_re, f"{sre} {data[tags[0]]} {ere}")
    temp = temp.replace(old_hi, f"{shi} {data[tags[1]]} {ehi}")
    temp = temp.replace(old_st, f"{sst} {data[tags[2]]} {est}")
    temp = temp.replace(old_no, f"{sno} {date.today().isoformat()} {eno}")
    update_index = open(f"{bwd}/index.html", 'w')
    update_index.write(temp)
    update_index.close()
    print("update index page successfully")



def create_new_project():
    os.chdir(bwd + "/projects")
    curr_dir = os.listdir()
    # To keep only the directory
    for item in curr_dir:
        if "." in item:
            curr_dir.remove(item)
    # To show list of projects category
    if len(curr_dir) <= 0:
        print("Please create project category first")
        return None
    for item in curr_dir:
        print(f"{curr_dir.index(item)} : {item}")
    category = int(input("Your Input: "))
    if category >= len(curr_dir):
        print("Invalid input")
        return None
    category = curr_dir[category]
    title = input("Title: ") 
    description = input("Description: ")
    git = input("Git: ")
    # File path
    title_url = "-".join(title.lower().split())
    # Detail of the new file
    file_detail = {'category': category, 'title': title, 'description': description, 'title_url': title_url, 'now': date.today().isoformat(), 'git': git}

    # Open the template file
    f_template = open(f"{bwd}/{data['template']}")
    new_file_path = f"{bwd}/projects/{category}/{file_detail['title_url']}.html"
    new_file = open(new_file_path, 'w')
    temp = f_template.read()
    for key, val in file_detail.items():
        temp = temp.replace("@{" + key + "}", val)
    new_file.write(temp)
    new_file.close()
    f_template.close()
    print(f"Generate {title} inside {category} successfully")
    print("Add to recent?\n1: Yes 0: No")
    option = input("Select: ")
    if option == "1":
        update_recent(category, f"{file_detail['title_url']}.html")


def update_conf():
    file = open(f"{bwd}/conf-gemini.json", 'w')
    file.write(json.dumps(data))
    file.close()
 

def edit_status():
    data['status'] = input("Status: ")
    update_conf()
    print("edit successfully")

def list_highlighted():
    highlighted = data['highlighted'].split("\n")
    if highlighted[len(highlighted) - 1] == '':
        highlighted.pop()
    print('\n' + "=="*10, "highlighted:", "=="*10)
    for item in highlighted:
        print(highlighted.index(item), ":", item[item.find('html') + 6:item.find('</a>')])
    return highlighted

def del_highlighted():
    highlighted = list_highlighted()
    remove = int(input("Delete Index: "))
    if remove < len(highlighted):
        highlighted.pop(remove)
        data['highlighted'] = "\n".join(highlighted)
        update_conf()
        print("Removed successfully")

def add_highlighted():
    highlighted = list_highlighted()
    proj = get_proj_lists()
    counter = 0
    print("\nCategory Lists:")
    for category, lists in proj:
        print(f"{counter}: {category}")
        counter += 1
    selected = int(input("Select Category: "))
    if selected < len(proj):
        for item in proj[selected][1]:
            print(f"{proj[selected][1].index(item)}: {item}")
        option = int(input("Select: "))
        if option < len(proj[selected][1]):
            category, project = proj[selected][0], proj[selected][1][option]
            temp = f"<li><a href='./projects/{category}/{project}'>{project[:project.find('.ht')]}</a></li>"
            highlighted.append(temp)
            data['highlighted'] = "\n".join(highlighted)
            update_conf()
            print("Added successfully")

def update_recent(category, project):
    recent = data['recent'].split("\n")
    recent.append(f"<li><a href='./projects/{category}/{project}'>{project[:project.find('.ht')]}</a></li>")
    if recent[len(recent) - 1] == '':
        recent.pop()
    while len(recent) > data['recent-limit']:
        recent.pop(0)
    data['recent'] = "\n".join(recent)
    print("Updated recent successfully")
    update_conf()


def update_all_projects():
    # Update the layout without updating the content section
    proj_template = read_file(f"{bwd}/projects/", "template.html")
    proj_lists = get_proj_lists()
    start_content, end_content = get_start_end_tags("content")
    template_content = get_content_w_tag(proj_template, start_content, end_content)
    for proj_cate, items in proj_lists:
        for proj in items:
            temp = read_file(f"{bwd}/projects/{proj_cate}/", proj)
            # get content of temp file
            file_content = get_content_w_tag(temp, start_content, end_content)
            # Get data
            temp_data = temp[temp.find("start_data"):temp.find("end_data")].split("|")
            temp_data.pop(0)
            temp_data.pop()
            print(temp_data)
            file_detail = {"title": temp_data[0], "category": temp_data[1], "now": temp_data[2], "git": temp_data[3]}
            # Replace the content / data to template
            new_file_content = proj_template
            new_file_content = new_file_content.replace(template_content, file_content)
            for key, val in file_detail.items():
                new_file_content = new_file_content.replace("@{" + key + "}", f"{val}")
            write_file(f"{bwd}/projects/{proj_cate}/", proj, new_file_content)
    print("Successfully update all projects template")

def create_note():
    proj_template = read_file(f"{bwd}/projects/", "template.html")
    start_content, end_content = get_start_end_tags("content")
    template_content = get_content_w_tag(proj_template, start_content, end_content)
    title = input("Title: ")
    ask = True
    content = f"{start_content} \n<section>\n <ul>"
    print("Enter stop() to stop the note")
    while ask:
        note = str(input('note: '))
        if note == "stop()":
            ask = False
        else:
            content += f"\n<li> {note} </li>"
    content += f"\n</ul>\n</section> {end_content}" 
    title_url = "-".join(title.lower().split())
    file_detail = {"title": title, "category": "note", "now": date.today().isoformat(), "title_url":f"{title_url}.html", "git": "#"}
    new_file_content = proj_template
    new_file_content = new_file_content.replace(template_content, content)
    for key, val in file_detail.items():
        new_file_content = new_file_content.replace("@{" + key + "}", f"{val}")
    write_file(f"{bwd}/projects/note/", f"{title_url}.html", new_file_content)
    print("Add to recent?\n1: Yes 0: No")
    option = input("Select: ")
    if option == "1":
        update_recent("note", f"{file_detail['title_url']}")
    print("Added note!")
 

menu = {'0': create_new_project, '1': edit_status, '2': add_highlighted, '3': del_highlighted, '4': update_projects_page, '5': update_index_page, '6': update_all_projects, '7': create_note }

repeat = True
while repeat:
    print("\nGemini 2021-09-15")
    for key, val in menu.items():
        print(f"Option {key}: {val.__name__}")
    your_option = input("Select: ")
    if your_option in menu:
        menu[your_option]()
        update_projects_page()
        update_index_page()
    else: 
        print("bye bye")
        repeat = False




