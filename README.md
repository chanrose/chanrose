# Chanrose

Just my personal static sites.

## Gemini

Static-site generator, latest feature:
- Create a new project within each category
- Update the all projects page, by counting the every directory under projects directory
- Add status update feature
- Update layout of the projects
- Enable to add recent / highlighted


### Finding

- It seems like on highlighted lists, it is better to use set instead of list in case when the project is already exist
- Should recent section check if the file actually exist or not?

## To Do
- ~~Projects.html @{now} should be replace with latest update~~
- ~~Home page change h3 to h2~~
- Reduce repetitive codes
- Change content 2 columns to 4 columns  on big screen
- ~~Make it pure all black / white~~
- Gemini: Manage projects direct - Delete single project and update index / projects
- Gemini: Check recent / highlighted if the file exist
- Gemini: Delete a project?

## What I familiar with:

- HTML & CSS
- JavaScript, JQuery, React
- Bootstrap, Ionic
- Git
- Python, Django
- !PHP, !Java 

## Recently

- Solidity & Truffle
- Vyper
- Brownie

## Favorite:

- [PopOS](https://pop.system76.com/)
